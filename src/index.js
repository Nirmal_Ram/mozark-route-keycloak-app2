import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import reportWebVitals from './reportWebVitals';
import Keycloak from 'keycloak-js';

let keycloak = Keycloak('./keycloak.json');
keycloak.init({onLoad:'login-required'}).success(authenticated=>{

ReactDOM.render(
  <React.StrictMode>
    {keycloak.authenticated ?
    <App keycloak={keycloak}/>
    :
    keycloak.login()
    }
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
})