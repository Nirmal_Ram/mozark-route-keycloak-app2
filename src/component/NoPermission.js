import React from 'react';

function NoPermission(props) {
    return (
        <div>
            <h3>Sorry you have No Permission to view this route . </h3>
        </div>
    );
}

export default NoPermission;