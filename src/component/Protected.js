import React from 'react';

function Protected(props) {
    return (
        <div>
            <h4>success : you are viewing the page for super-admin [highly protected route] </h4>
        </div>
    );
}

export default Protected;