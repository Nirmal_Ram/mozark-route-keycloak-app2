import React from 'react';

function Unprotected(props) {
    return (
        <div>
            <h4>This is unprotected route . no need of roles to get the access to this page. it's always FREE</h4>
        </div>
    );
}

export default Unprotected;